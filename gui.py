# -*- coding: UTF-8 -*-

import Tkinter as tk
from main import Movie
from chart import Chart
from graph import Graph, Graph2, Graph3
import guim
import guid
from PIL import Image, ImageDraw, ImageFont,ImageTk
root = tk.Tk()
root.title("电影票房数据获取分析器")
root.iconbitmap('movie.ico')
global c
global ctsj
global ctsj2
global ctsj1in
global ctsj2in
def getgraph():
    global ctsj
    Graph(ctsj)

def getgraph2():
    global ctsj
    Graph2(ctsj)
def getgraph3():
    global ctsj
    global ctsj2
    global ctsj2in
    global ctsj1in
    print ctsj
    print ctsj2
    Graph3(ctsj,ctsj2,ctsj1in,ctsj2in)

def getchart():
    global c
    c=Chart()
    for content in c.chart:
        LB.insert(tk.END, content["title"]+"                "+content["infor"][0])


def getguim():
    a = LB.curselection()
    a=a[0]
    url = c.chart[a]["href"]

    mov = Movie(url)
    d=guim.guim(mov)

    child = tk.Toplevel()
    child.title("影片基本信息")
    child.iconbitmap('movie.ico')
    lb = tk.Label(child, text = "影片基本信息")
    lb.pack()

    image = Image.open("basic.png")
    photo = ImageTk.PhotoImage(image)
    p = tk.Text(child,width = 40, height = 20)
    p.image_create(tk.END, image = photo)
    p.pack()

    child.mainloop()

def getguidd():

    a = LB.curselection()
    a=a[0]

    url = c.chart[a]["href"]
    global ctsj1in
    global ctsj2in
    global ctsj
    ctsj=[]
    mov = Movie(url)
    for content in mov.daily:
        ctsj.append(int(filter(str.isdigit, content["Gross-to-Date"])))
    ctsj1in = mov.fulltitle

    url2 = c.chart[0]["href"]
    global ctsj2
    ctsj2=[]
    mov2 = Movie(url2)
    for content in mov2.daily:
        ctsj2.append(int(filter(str.isdigit, content["Gross-to-Date"])))
    ctsj2in = mov2.fulltitle

    getgraph3()


def getguid():
    a = LB.curselection()
    a=a[0]
    url = c.chart[a]["href"]

    mov = Movie(url)
    d=guid.guid(mov)

    child = tk.Toplevel()
    child.title("影片日票房信息")
    child.iconbitmap('movie.ico')
    lb = tk.Label(child, text = "影片日票房信息", font = 50)
    lb.pack()

    global ctsj
    ctsj=[]

    for content in mov.daily:
        ctsj.append(int(filter(str.isdigit, content["Gross-to-Date"])))



    bt = tk.Button(child, text = "查看该影片累计票房折线图",command = getgraph)
    bt.pack()

    bt2 = tk.Button(child, text = "查看该影片每日票房折线图",command = getgraph2)
    bt2.pack()

    sb = tk.Scrollbar(child)
    sb.pack(side = tk.RIGHT, fill = tk.Y)

    image = Image.open("daily.png")
    photo = ImageTk.PhotoImage(image)
    p = tk.Text(child,height = 1000 , width = 145 ,yscrollcommand = sb.set)
    p.image_create(tk.END, image = photo)
    p.pack()
    sb.config(command = p.yview)
    child.mainloop()


tk.Label(root, text = "点击右侧获取最新信息").grid(row = 0,column = 0)
b1 = tk.Button(root, text = "获取", command = getchart)
b1.grid(row = 0 , column = 1)

tk.Label(root, text = "以下是上周末票房榜：").grid(row = 1,column = 0,columnspan = 2)

tk.Label(root, text = "片名      -      上周末票房").grid(row = 2,column = 0,columnspan = 2)

LB = tk.Listbox(root,width = 50,height = 20)
LB.grid(row = 3 , column = 0)

b3 = tk.Button(root, text = "查看该影片基础信息" , command = getguim)
b3.grid(row = 4 , column = 0)
b3 = tk.Button(root, text = "查看该影片票房信息", command = getguid)
b3.grid(row = 5 , column = 0)
b3 = tk.Button(root, text = "查看该影片票房对比信息", command = getguidd)
b3.grid(row = 6 , column = 0)

root.mainloop()
tl1 = tk.Toplevel()
