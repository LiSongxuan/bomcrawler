# -*- coding: UTF-8 -*-
from prettytable import PrettyTable
from PIL import Image, ImageDraw, ImageFont,ImageTk
from main import Movie
import Tkinter as tk
class guim(object):
    def __init__(self,url):
        self.starts(url)
    def starts(url,movi):
        tab = PrettyTable()
        # 设置表头
        tab.field_names = ["信息", "数据"]
        # 表格内容插入

        tab.add_row(['出品方', movi.basic["Distributer"]])
        tab.add_row(['上映日期', movi.basic["Release_Date"]])
        tab.add_row(['类型', movi.basic["Genre"]])
        tab.add_row(['片长', movi.basic["Runtime"]])
        tab.add_row(['MPAA分级', movi.basic["MPAA_Rating"]])
        tab.add_row(['制作预算', movi.basic["Production Budget"]])
        tab_info = str(tab)
        space = 5

        # PIL模块中，确定写入到图片中的文本字体
        font = ImageFont.truetype('simfang.ttf', 15, encoding='utf-8')
        # Image模块创建一个图片对象
        im = Image.new('RGB',(10, 10),(0,0,0,0))
        # ImageDraw向图片中进行操作，写入文字或者插入线条都可以
        draw = ImageDraw.Draw(im, "RGB")
        # 根据插入图片中的文字内容和字体信息，来确定图片的最终大小
        img_size = draw.multiline_textsize(tab_info, font=font)
        # 图片初始化的大小为10-10，现在根据图片内容要重新设置图片的大小
        im_new = im.resize((img_size[0]+space*2-100, img_size[1]+space*2))
        del draw
        del im
        draw = ImageDraw.Draw(im_new, 'RGB')
        # 批量写入到图片中，这里的multiline_text会自动识别换行符
        draw.multiline_text((space,space), unicode(tab_info, 'utf-8'), fill=(255,255,255), font=font)
        im_new.save('basic.PNG', "PNG")
        del draw
class Ui(object):
    def __init__(self,url):
        mov = Movie(url)
        d=guim(mov)

        root = tk.Tk()
        lb = tk.Label(root, text = "影片基本信息")
        lb.pack()
        #sb = tk.Scrollbar(root)
        #sb.pack(side = tk.RIGHT, fill = tk.Y)
        #sb.grid(row = 1,column = 1)
        image = Image.open("basic.png")
        photo = ImageTk.PhotoImage(image)
        p = tk.Text(root,width = 40, height = 20)
        p.image_create(tk.END, image = photo)
        p.pack()
        #sb.config(command = p.yview)
        root.mainloop()
