# -*- coding: UTF-8 -*-

import matplotlib.pyplot as plt
from pylab import mpl

class Graph(object):
    def __init__(self,a = []):
        mpl.rcParams['font.sans-serif'] = ['SimHei'] # 指定默认字体
        mpl.rcParams['axes.unicode_minus'] = False # 解决保存图像是负号'-'显示为方块的问题
        self.sj = a
        gp = plt.figure(dpi = 128, figsize =(10,6))
        plt.ticklabel_format(style = 'plain')
        plt.plot(self.sj)
        plt.title(u'电影累计票房示意图', fontsize = 24)
        plt.xlabel(u'上映天数' ,fontsize = 16)
        plt.ylabel(u'票房/美元', fontsize = 16)
        plt.show()

class Graph2(object):
    def __init__(self,a = []):
        mpl.rcParams['font.sans-serif'] = ['SimHei'] # 指定默认字体
        mpl.rcParams['axes.unicode_minus'] = False # 解决保存图像是负号'-'显示为方块的问题

        self.sj = a
        b = [self.sj[1]]

        for i in range(1,len(self.sj)-1):
            b.append(self.sj[i]-self.sj[i-1])

        gp = plt.figure(dpi = 128, figsize =(10,6))
        plt.ticklabel_format(style = 'plain')
        plt.plot(b)
        plt.title(u'电影每日票房示意图', fontsize = 24)
        plt.xlabel(u'上映天数' ,fontsize = 16)
        plt.ylabel(u'票房/美元', fontsize = 16)
        plt.show()

class Graph3(object):
    def __init__(self,a = [],b = [],c='',d=''):
        mpl.rcParams['font.sans-serif'] = ['SimHei'] # 指定默认字体
        mpl.rcParams['axes.unicode_minus'] = False # 解决保存图像是负号'-'显示为方块的问题

        self.sj1 = a
        aa = self.sj1
        self.sj2 = b
        bb = self.sj2
        print aa

        gp = plt.figure(dpi = 128, figsize =(10,6))
        plt.ticklabel_format(style = 'plain')
        plot1 = plt.plot(aa,label=c)
        plot2 = plt.plot(bb,label=d)
        plt.title(u'电影每日票房示意图', fontsize = 24)
        plt.xlabel(u'上映天数' ,fontsize = 16)
        plt.ylabel(u'票房/美元', fontsize = 16)
        plt.legend()
        plt.show()
