# -*- coding: UTF-8 -*-
from prettytable import PrettyTable
from PIL import Image, ImageDraw, ImageFont, ImageTk
from main import Movie
import Tkinter as tk
class guid(object):
    def __init__(self,mov):
        self.starts(mov)
    def starts(self,movi):
        tab = PrettyTable()
        # 设置表头
        tab.field_names = ["星期", "日期","排名","当日票房","环比前一日","同比上周","上映影院数","平均每影院票房","总票房","上映天数"]
        # 表格内容插入
        for i in movi.daily:
            tab.add_row([i["Day"],i["Date"],i["Rank"],i['Gross'],i['DayChange'],i['WeekChange'],i['Theaters'],i['AvgTheater'],i['Gross-to-Date'],i['Daynum']])
            tab_info = str(tab)
            space = 5

        # PIL模块中，确定写入到图片中的文本字体
        font = ImageFont.truetype('simfang.ttf', 15, encoding='utf-8')
        # Image模块创建一个图片对象
        im = Image.new('RGB',(10, 10),(0,0,0,0))
        # ImageDraw向图片中进行操作，写入文字或者插入线条都可以
        draw = ImageDraw.Draw(im, "RGB")
        # 根据插入图片中的文字内容和字体信息，来确定图片的最终大小
        img_size = draw.multiline_textsize(tab_info, font=font)
        # 图片初始化的大小为10-10，现在根据图片内容要重新设置图片的大小
        im_new = im.resize((img_size[0]+space*2-1000, img_size[1]+space*2))
        del draw
        del im
        draw = ImageDraw.Draw(im_new, 'RGB')
        # 批量写入到图片中，这里的multiline_text会自动识别换行符
        draw.multiline_text((space,space), unicode(tab_info, 'utf-8'), fill=(255,255,255), font=font)
        im_new.save('daily.PNG', "PNG")
        del draw

class Ui(object):
    def __init__(self,url):
        mov = Movie(url)
        d=guid(mov)

        root = tk.Tk()
        lb = tk.Label(root, text = "影片日票房信息", font = 50)
        lb.pack()
        sb = tk.Scrollbar(root)
        sb.pack(side = tk.RIGHT, fill = tk.Y)
        #sb.grid(row = 1,column = 1)
        image = Image.open("daily.png")
        photo = ImageTk.PhotoImage(image)
        p = tk.Text(root,height = 1000 , width = 145 ,yscrollcommand = sb.set)
        p.image_create(tk.END, image = photo)
        p.pack()
        sb.config(command = p.yview)
        root.mainloop()
