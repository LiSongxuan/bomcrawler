import requests
import thread
from bs4 import BeautifulSoup
import re
daily_base_url = 'http://www.boxofficemojo.com/movies/?page=daily&view=chart&'
daily_data_range = ['Day', 'Date', 'Rank', 'Gross', 'DayChange', 'WeekChange', 'Theaters', 'AvgTheater', 'Gross-to-Date', 'Daynum']
basic_data_range = ['Distributer', 'Release Date', 'Genre', 'Runtime', 'MPAA Rating', 'Production Budget']
basic_base_url = 'http://www.boxofficemojo.com/movies/?'

class Movie(object):

    def __init__(self,href):
        self.mtitle = self.geturl(href)
        self.fulltitle = ''
        self.domgross = 0
        self.wdwd = 0
        self.daily = []
        self.basic = {}
        self.getsdaily(self.mtitle)
        self.getsbasic(self.mtitle)

    def geturl(self,href):
        m=r'id=.+?\.htm'
        aurl=re.compile(m)
        alist=re.findall(aurl,href)
        return alist[0]


    def clean_html(self, soup):
        """Get rid of all bold, italic, underline and link tags"""
        invalid_tags = ['b', 'i', 'u', 'nobr', 'font' , 'br']
        for tag in invalid_tags:
            for match in soup.findAll(tag):
                match.replaceWithChildren()

    def clean_html_basic(self, soup):
        """Get rid of all bold, italic, underline and link tags"""
        invalid_tags = ['b', 'i', 'u', 'nobr', 'font' , 'br' , 'table' , 'center' , 'tr' , 'td']
        for tag in invalid_tags:
            for match in soup.findAll(tag):
                match.replaceWithChildren()


    def data_anyss(self, soup):
        data={}
        tim=0
        for content in soup.contents:
            try:
                p = content.text
                data[daily_data_range[tim]]=p.encode('utf-8')
                tim = tim + 1
            except:
                continue
        return data

    def getstitle(self,asoup):
        soup=asoup.find("title" ).text
        texting = soup
        texting = re.sub(' - Box Office Mojo', '', texting)
        texting = re.sub(r'\([0-9]{4,}\)', '', texting)
        self.fulltitle = texting

    def getsbasic(self,movie_id):
        url = basic_base_url + movie_id
        print url
        page = requests.get(url)

        soup = BeautifulSoup(page.content, "html.parser")
        self.getstitle(soup)
        soup = soup.findAll("td", { "valign" : "top" , "align" : "center" })
        soup = soup[1]
        self.clean_html(soup)
        for content in soup.contents:
            if content.name is None:
                #self.fulltitle = self.fulltitle + content.string
                break
            else:
                soup = content

        self.clean_html_basic(soup)
        basic_all={}
        #status_code1 = 0
        try:
            basic_all["Domestic Total"] = soup.contents[3].encode('utf-8')
        except:
            pass

        try:
            basic_all["Distributer"] = soup.contents[5].text.encode('utf-8')
        except:
            pass
        try:
            basic_all["Release_Date"] = soup.contents[7].text.encode('utf-8')
        except:
            pass
        try:
            basic_all["Genre"] = soup.contents[9].encode('utf-8')
        except:
            pass
        try:
            basic_all["Runtime"] = soup.contents[11].encode('utf-8')
        except:
            pass
        try:
            basic_all["MPAA_Rating"] = soup.contents[13].encode('utf-8')
        except:
            pass
        try:
            basic_all["Production Budget"] = soup.contents[15].encode('utf-8')
        except:
            pass

        self.basic = basic_all


    def getsdaily(self,movie_id):
        data_all=[]
        tim=1

        url = daily_base_url + movie_id
        page = requests.get(url)

        soup = BeautifulSoup(page.content, "html.parser")

        soup = soup.find("table", { "class" : "chart-wide" })
        self.clean_html(soup)

        for content in soup.contents:
            try:
                data_all.append(self.data_anyss(content))
            except:
                pass

        data_all.pop(0)

        data_pp=[]

        for i in data_all:
            if i['Day'] is not "":
                data_pp.append(i)
        self.daily = data_pp
